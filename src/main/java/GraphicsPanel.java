import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraphicsPanel extends JPanel implements Runnable{

    private static final long serialVersionUID = 6791888953399961746L;

    private Line line;

    public GraphicsPanel() {
        double y = (double)((int)(Math.random() * 788)) ;
        line = new Line(0, y, 0, y + 50);

        //(new Thread(this)).start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.draw(line);
    }

    public void rerepaint() {
        super.repaint();
    }

    @Override
    public void run() {
        final Timer t = new Timer(20, null);
        ActionListener performer = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                line.move();
                rerepaint();
            }
        };
        t.addActionListener(performer);
        t.start();
    }
}
