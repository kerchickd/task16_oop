import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setTitle("Points move");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final Timer t = new Timer(1000, null);
        ActionListener performer = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphicsPanel graphicsPanel = new GraphicsPanel();

                frame.add(graphicsPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                        GridBagConstraints.NORTH, GridBagConstraints.BOTH,
                        new Insets(2, 2, 2, 2), 0, 0));
                (new Thread(graphicsPanel)).start();
            }
        };
        t.addActionListener(performer);
        t.start();
        frame.setVisible(true);
    }
}
//1550 838